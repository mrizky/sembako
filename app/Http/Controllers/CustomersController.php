<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Customer;

use View;
use Validator;

class CustomersController extends Controller
{
    private $page_title;
    private $controller;
    private $model;
    private $view;
    private $main_model;

    public function __construct(Customer $main_model) 
    {
        $no = 1;
        $this->page_title = "Customers";
        $this->controller = "Customers";
        $this->model      = "Customer";
        $this->view       = "customers";
        $this->main_model = $main_model;

        View::share('no', $no);
        View::share('page_title', $this->page_title);
        View::share('controller', $this->controller);
        View::share('model', $this->model);
        View::share('view', $this->view);
        View::share('main_model', $this->main_model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = $this->main_model->all();
        return view($this->view.'.index')
            ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input, Customer::$rules);

        if ($validation->passes()) {
            $this->main_model->create($input);
            return redirect()->route($this->view.'.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->main_model->findOrFail($id);
        return view($this->view.'.show')
            ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->main_model->findOrFail($id);
        return view($this->view.'.edit')
            ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data  = $this->main_model->findOrFail($id);
        $input = $request->all();
        
        $data->fill($input)->save();
        return redirect()->route($this->view.'.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->main_model->findOrFail($id);
        $data->delete();
        return redirect()->route($this->view.'.index');
    }
}
