<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'id',
        'name',
        'email',
        'phone_number',
        'addres',
        'created_at',
        'updated_at'
    ];

    protected $dates = ['deleted_at'];

    public static $rules = [];
}
