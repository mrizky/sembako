<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>NusiMedia Admin Page</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{ asset('/adminlte/bootstrap/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/skins/skin-blue.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/bootstrap3-wysihtml5.min.css') }}">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/skins/_all-skins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
                ]); ?>
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        @include('layouts.header')
        @include('layouts.side')
        <div class="content-wrapper">
            <section class="content">
                @yield('content')
            </section>
        </div>
        @include('layouts.footer')

        <!-- jQuery 2.2.3 -->
        <script src="{{ asset('/adminlte/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="{{ asset('/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- Slimscroll -->
        <script src="{{ asset('/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <!-- FastClick -->
        <script src="{{ asset('/adminlte/plugins/fastclick/fastclick.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('/adminlte/dist/js/app.min.js') }}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{ asset('/adminlte/dist/js/demo.js') }}"></script>
        <script src="{{ asset('/adminlte/dist/js/selectize.js') }}"></script>
        <script src="{{ asset('/adminlte/dist/js/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <script src="{{ asset('/datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
        <script src="{{ asset('/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js') }}"></script>
        <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
        <script type="text/javascript">

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#showImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#inputImage").change(function () {
                readURL(this);
            });
            $(function () {
                $(".textarea").wysihtml5();
            });
            $('#tags').selectize({
                persist: false,
                createOnBlur: true,
                create: true
            });
            $('#title').keyup(function(){
                var value = $(this).val();
                value = value.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
                $('#slug').val(value);
            });
            $('#name').keyup(function(){
                var value = $(this).val();
                value = value.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
                $('#slug').val(value);
            });
            $('.datepicker').datetimepicker({
                format: 'yyyy-mm-dd'
            });
            $(function () {
              $(".timepicker").timepicker({
                showMeridian: false
              });
            });
            $('.datetimepicker').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss'
            });
        </script>

    </body>
</html>
