<table class="table table-responsive" id="banners-table">
    <thead>
        <th>parent_id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($datas as $data)
        <tr>
            <td>{!! @$data->category_banner->name!!}</td>
            <td>{!! $data->name!!}</td>
            <td>
                {!! Form::open(['route' => [$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route($view.'.show', [$data->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route($view.'.edit', [$data->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
