@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {{$page_title}}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => $location.'.'.$view.'.store', 'files' => true]) !!}

                        @include($location.'.'.$view.'.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
