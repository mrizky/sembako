@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {{$page_title}}
        </h1>
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($data, ['route' => [$location.'.'.$view.'.update', $data->id], 'method' => 'patch', 'files' => true]) !!}

                        @include($location.'.'.$view.'.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
