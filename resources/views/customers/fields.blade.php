<div class="form-group col-sm-6">

    <!-- Name Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', Null,['class' => 'form-control']) !!}
    </div>

    <!-- Name Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('subtitle', 'Subtitle:') !!}
        {!! Form::text('subtitle', Null,['class' => 'form-control']) !!}
    </div>

    <!-- Picture Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('picture', 'Picture:') !!}
        {!! Form::file('picture',['class' => 'form-control','id'=>'inputImage']) !!}
    </div>
    <div class="form-group col-sm-12">
        <img id="showImage" class="img-responsive" src="{{url('uploads/'.@$data->picture)}}"/>
    </div>

</div>
<div class="form-group col-sm-6">

  <!-- content Field -->
  <div class="form-group col-sm-12">
      {!! Form::label('content', 'Content:') !!}
      {!! Form::textarea('content', null, ['class' => 'textarea']) !!}
  </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route($location.'.'.$view.'.index') !!}" class="btn btn-default">Cancel</a>
</div>
