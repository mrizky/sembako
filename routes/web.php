<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'HomeController@index');

    Route::group(['middleware' => ['auth']], function () {

        //  
        Route::resource('customers', 'CustomersController');
        
            //Post
            // Route::resource('posts', 'PostsController', ['as'=>'backend']);
            // Route::resource('category_post','CategoryPostsController',['as'=>'backend']);
            // Route::get('posts/publish/{id}', ['as' => 'backend.posts.publish', 'uses' => 'PostsController@publish']);
            // Route::get('posts/unpublish/{id}', ['as' => 'backend.posts.unpublish', 'uses' => 'PostsController@unpublish']);
            // Route::get('posts/index_writer', ['as' => 'backend.posts.index_writer', 'uses' => 'PostsController@index_writer'])   

    });

    Auth::routes();

});
